package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    // FIXME: You need to write tests for the two known bugs in the code.
    @Test
    public void testTestingBoxComplete() {
        logger.info("Test of box completion test");
        logger.info("Drawing complete box, then asserting complete state");
        DotsAndBoxesGrid g = new DotsAndBoxesGrid(4,4,1);
        g.drawHorizontal(1,1, 1);
        g.drawHorizontal(1,2,1);
        g.drawVertical(1,1,1);
        g.drawVertical(2,1,1);
        assertEquals(true,g.boxComplete(1,1));
    }

    @Test
    public void testTestingLineDrawn() {
        logger.info("Test of line already drawn detection");
        logger.info("Instantiating new grid");
        DotsAndBoxesGrid h = new DotsAndBoxesGrid(4,4,1);
        logger.info("Drawing a line to later be duplicated");
        h.drawHorizontal(1,1,1);
        logger.info("Drawing line a second time within an assert throws block, expecting and illegal state exception");
        IllegalStateException thrown = assertThrows(
                IllegalStateException.class, () -> h.drawHorizontal (1,1,1),
                "Drawing second horizontal line should have thrown illegal state exception, did not"
        );
        assertTrue(thrown.getMessage().contains("IllegalStateException"));

    }
}